//Kevin Echenique Arroyo #1441258

package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	//CONSTRUCTOR
	public Vector3d(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	//GET METHODS
	public double getX() {
		return this.x;
	}
	public double getY() {
		return this.y;
	}
	public double getZ() {
		return this.z;
	}
	
	//Calculates the magnitude
	public double magnitude() {
		double sumAndSquare = (Math.pow(this.x, 2)) + (Math.pow(this.y, 2)) + (Math.pow(this.z, 2)); 
		double calMagnitude = Math.sqrt(sumAndSquare);
		return calMagnitude;
	}
	
	//Takes two vectors and returns the dot product of said vectors
	public double dotProduct(Vector3d vec) {
		double valX = vec.getX()*this.x;
		double valY = vec.getY()*this.y;
		double valZ = vec.getZ()*this.z;
		
		double multAndAdd = (valX) + (valY) + (valZ);
		return multAndAdd;
	}
	
	//Takes two vectors and returns the sum of corresponding variables
	public Vector3d add(Vector3d vec) {
		double valX = vec.getX()+this.x;
		double valY = vec.getY()+this.y;
		double valZ = vec.getZ()+this.z;
		
		//Creates a new Vector3d with the new values and returns it
		Vector3d newVector = new Vector3d(valX, valY, valZ);
		return newVector;
	}
	
	

}
