//Kevin Echenique Arroyo #1441258
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	//Testing get methods
	@Test
	void testGetMethods() {
		Vector3d vec = new Vector3d(2, 4 ,6);
		assertEquals(2, vec.getX());
		assertEquals(4, vec.getY());
		assertEquals(6, vec.getZ());
	}
	
	//Testing magnitude() method
	@Test
	void testMagnitudeMethod() {
		Vector3d vec1 = new Vector3d(2, 4, 6);
		assertEquals(7.483314773547883, vec1.magnitude());
	}
	
	//Testing dotProduct() method
	@Test
	void testDotProduct() {
		Vector3d vec1 = new Vector3d(2, 4, 6);
		Vector3d vec2 = new Vector3d(1, 3, 5);
		assertEquals(44, vec1.dotProduct(vec2));
	}
	
	//Testing add() method
	@Test
	void testAdd() {
		Vector3d vec1 = new Vector3d(1, 2, 3);
		Vector3d vec2 = new Vector3d(4, 5, 6);
		Vector3d vec3 = vec1.add(vec2);
		double x = vec3.getX();
		double y = vec3.getY();
		double z = vec3.getZ();
		assertEquals(5, x);
		assertEquals(7, y);
		assertEquals(9, z);
	}

}
