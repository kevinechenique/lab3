//Kevin Echenique Arroyo #1441258
package LinearAlgebra;

public class Test {

	public static void main(String[] args) {
		Vector3d vec1 = new Vector3d(2, 4, 6);
		Vector3d vec2 = new Vector3d(1, 3, 5);
		System.out.println(vec1.magnitude()); //magnitude METHOD PASSED!
		
		/*ALL THIS CODE BELOW IS NOT THE CORRECT WAY TO SOLVE IT ALTHOUGH IT WORKS. REMEMBER THAT INSTANT METHODS ALREADY HAVE ONE IMPLICIT OBJECT INCLUDED EXAMPLE: vec1.addNum(vec2)
		 * To solve this I chose to modify the instance methods inside my Vector3d class
		
		double calDotProduct;
		calDotProduct = new Vector3d(0, 0, 0).dotProduct(vec1, vec2); //one of two alternatives, this Object that we created can have whatever input. It will not affect the result.
		calDotProduct = vec1.dotProduct(vec1, vec2);//The second alternative. We can use either vec1 or vec2, that does not affect the result. The reason being that the object we use to load the method (vec1 in this case), will take the value produced by that method which ends up stored inside the double calDotProduct
		System.out.println(calDotProduct);
		*/
		
		//dotProduct METHOD PASSED!
		double calDotProduct = vec1.dotProduct(vec2);
		System.out.println(calDotProduct);
		
		//add() METHOD PASSED!
		Vector3d vec3 = vec1.add(vec2);//vec3 stores the result of the object returned by the add() method
		double x = vec3.getX();
		double y = vec3.getY();
		double z = vec3.getZ();
		System.out.println(x +" "+ y +" "+ z);
		
		
	}

}
